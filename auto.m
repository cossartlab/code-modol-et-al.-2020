%%make sure you have cvx installed if you choose to get spikes via MCMC
%%(http://cvxr.com/cvx/download/)
%%use this version of normcorre and caiman or download again (you may need
%%to add some functions)


%load data
clear
gcp;
[FileName,PathName,FilterIndex]=uigetfile('*.tif');
addpath(PathName);

tic; Y = read_file(FileName); toc; % read the file (optional, you can also pass the path in the function instead of Y)
Y = single(Y);      % convert to double precision 
[d1,d2,T] = size(Y);                                % dimensions of dataset
d = d1*d2;                                          % total number of pixels
refine_components = true;  % flag for manual refinement (add cells and such)
mansec=1;   %flag for manual rejection


%%
%motion correction rigid
options_rigid = NoRMCorreSetParms('d1',size(Y,1),'d2',size(Y,2),'bin_width',200,'max_shift',30,'us_fac',50,'init_batch',200,'iter',2);
tic; [M1,shifts1,template1] = normcorre(Y,options_rigid); toc
%clearvars Y do these steps if you run out of memory or check the caiman
%memorymapped example
implay(mat2gray(M1));


options_rigid = NoRMCorreSetParms('d1',size(M1,1),'d2',size(M1,2),'bin_width',200,'max_shift',15,'us_fac',50,'init_batch',200);
tic; [M2,shifts2,template2] = normcorre(M1,options_rigid); toc
%clearvars M1
implay(mat2gray(M2));


%motion correction non-rigid
options_nonrigid = NoRMCorreSetParms('d1',size(M2,1),'d2',size(M2,2),'grid_size',[156,156],'mot_uf',4,'bin_width',200,'max_shift',15,'max_dev',[4,4,1],'us_fac',50,'init_batch',300,'boundary','copy','overlap_pre',[32,32,16],'iter',1,'use_parallel',true,'buffer_width',150,'correct_bidir',false,'shifts_method','FFT');
tic; [M4,shifts4,template4] = normcorre(M2,options_nonrigid); toc
%clearvars M3
implay(mat2gray(M4));

options_nonrigid = NoRMCorreSetParms('d1',size(M4,1),'d2',size(M4,2),'grid_size',[128,128],'mot_uf',4,'bin_width',200,'max_shift',10,'max_dev',[4,4,1],'us_fac',30,'init_batch',300,'boundary','copy','overlap_pre',[32,32,16],'iter',2,'use_parallel',true,'buffer_width',150,'correct_bidir',false,'shifts_method','FFT');
tic; [M5,shifts5,template5] = normcorre(M4,options_nonrigid); toc
%clearvars M4
implay(mat2gray(M5));


M5=Y;
XX=0;
M5(isnan(M5))=XX;
M5(M5<0)=XX;


[d1,d2,T] = size(M5);                                % dimensions of dataset
d = d1*d2;                                          % total number of pixels
%save movie
SS = FileName(1:end-4);
SSS='Corrected_Movie.mat';
St=strcat(SS,SSS);
save(St,'M5','shifts5','-v7.3') %add other variables like M1 etc.. if you want to save them
%%
%start extraction

K = 10;                                           % number of components to be found
tau = 4;                                          % std of gaussian kernel (half size of neuron) 
p = 0;                                             %order of autoregressive model to use eg 0,1,2

options = CNMFSetParms(...   
    'fr',3.3,...                                  %frame rate in HZ
    'decay_time',1.1,...                          %decay time in seconds (fs*decay time should be physical decay of transient)
    'ssub',1,...                                 %downsample spatial
    'tsub',2,...                                 %downsample  time
    'd1',d1,'d2',d2,...                         % dimensionality of the FOV
    'p',p,...                                   % order of AR dynamics    
    'gSig',tau,...                              % half size of neuron
    'merge_thr',0.95,...                        % merging threshold  
    'deconv_method','constrained_foopsi',...
    'nb',3,...                                  % number of background components    
    'min_SNR',3,...                             % minimum SNR threshold
    'space_thresh',0.2,...                      % space correlation threshold
    'cnn_thr',0.2...                            % threshold for CNN classifier (not used)    
    );
 
%% fast initialization of spatial components using greedyROI and HALS

[Ain,Cin,bin,fin,center] = initialize_components(Y,K,tau,options,P);  % initialize

% display centers of found components
Cn =  correlation_image(Y); %reshape(P.sn,d1,d2);  %max(Y,[],3); %std(Y,[],3); % image statistic (only for display purposes)
figure;imagesc(Cn);
    axis equal; axis tight; hold all;
    scatter(center(:,2),center(:,1),'mo');
    title('Center of ROIs found from initialization algorithm');
    drawnow;


    %% manually refine components (optional)
if refine_components
    [Ain,Cin,center]=manually_refine_components(Y,Ain,Cin,center,Cn,tau,options);
end

%% update spatial components
Yr = reshape(Y,d,T);
[A,b,Cin] = update_spatial_components(Yr,Cin,fin,[Ain,bin],P,options);

%% update temporal components
P.p = 0;    % set AR temporarily to zero for speed
[C,f,P,S,YrA] = update_temporal_components(Yr,A,b,Cin,fin,P,options);

%% classify components

%rval_space = classify_comp_corr(Y,A,C,b,f,options);
%ind_corr = rval_space > options.space_thresh;           % components that pass the correlation test


%% event exceptionality

%fitness = compute_event_exceptionality(C+YrA,options.N_samples_exc,options.robust_std);
%ind_exc = (fitness < options.min_fitness);

%keep = ind_corr & ind_exc;

%A_keep = A(:,keep);
%C_keep = C(keep,:);

[Am,Cm,K_m,merged_ROIs,Pm,Sm] = merge_components(Yr,A,b,C,f,P,S,options);

Pm.p = 0;    % restore AR value
[A2,b2,C2] = update_spatial_components(Yr,Cm,f,[Am,b],Pm,options);
[C2,f2,P2,S2,YrA2] = update_temporal_components(Yr,A2,b2,C2,f,Pm,options);



%% do some plotting
[A_or,C_or,S_or,P_or] = order_ROIs(A2,C2,S2,P2); % order components
K_m = size(C_or,1);
[C_df,~] = extract_DF_F_new(A_or,C_or,b2,f2,P_or,options); % extract DF/F values (optional)
%add option
C_df=full(C_df);
[Coor,json_file] = plot_contours(A_or,Cn,options,1); % contour plot of spatial footprints


%%
%make trace selection
if mansec==1
i=1;
close all;

Tracecells=[];
ContoursSoma={};
ContoursPeri={};
ContoursShit={};
TraceNeuro=[];
TraceNoise=[];
Tracecellsdf=[];
TraceNeurodf=[];
TraceNoisedf=[];
MemberShip=[];
BW4=zeros(d1,d2);
figure('units','normalized','outerposition',[0 0 1 1])
hold on;
while i<=length(C_or(:,1))
[d1,d2] = size(Cn);
cont = medfilt1(Coor{i}')';
Cn2=Cn;
BW1=zeros(d1,d2);
BW2=zeros(d1,d2);
for j=1:size(cont,2)
BW1(cont(2,j),cont(1,j))=1;
 se1 = strel('line',2,90);
    se2 = strel('line',2,0);
    BWtemp=imdilate(BW1,[se1,se2]);
        BWtemp= imfill(BWtemp,'holes');
 BW2=bwperim(BWtemp);
end
subplot(6,2,1)
subplot(6,2,[1 2 3 4 5 6 7 8 9 10])
imshow(adapthisteq((Cn2)));
hold on;
B2=bwboundaries(BW4);
visboundaries(B2,'LineWidth',0.2,'Color','g')
B = bwboundaries(BW2);
visboundaries(B,'LineWidth',0.2)
subplot(6,2,[11 12])
plot(C_df(i,:));
[x,y,z] = ginput(1);
if z==1
Tracecells(end+1,:)=C_or(i,:);
Tracecellsdf(end+1,:)=C_df(i,:);
ContoursSoma{end+1}=cont;
BW4=BW1+BW4;
end
if z==3
TraceNeuro(end+1,:)=C_or(i,:);
TraceNeurodf(end+1,:)=C_df(i,:);
ContoursPeri{end+1}=cont;
end
if z==2
TraceNoise(end+1,:)=C_or(i,:);
TraceNoisedf(end+1,:)=C_df(i,:);
ContoursPeri{end+1}=cont;
end

if z==103
i=i-2;
end

MemberShip(i)=z;
i=i+1;
end

save(St,'Tracecells', 'TraceNeuro','TraceNoise','Tracecellsdf', 'TraceNeurodf','TraceNoisedf','MemberShip','Coor','-mat','-v7.3');
C_df=Tracecellsdf;
C_or=Tracecells;
Coor=ContoursSoma;
end


%%
%plotting
figure;
imshow(Cn)
hold on;

for i=1:length(Coor)
cont = medfilt1(Coor{i}')';
for j=1:size(cont,2)
 BW1(cont(2,j),cont(1,j))=1;
 BW2=bwperim(BW1);
end
B = bwboundaries(BW2);
visboundaries(B,'Color','r','LineWidth', 0.2)
end




%%
%extract spikes
SAMPLES = cont_ca_sampler(C_df(5,:));    %% MCMC example for cell 5   
plot_continuous_samples(SAMPLES,C_df(5,:));
%MCMC
att={};
parfor i = 1:length(C_df(:,1))
SAMPLES = cont_ca_sampler(C_df(i,:)*(-1));    %% MCMC
ans2=SAMPLES.ss{end};
att{i}=sort(ans2);
end


%%
%%make spikes double time (eg. ac)
d1=length(C_or(:,1));
d2=2*length(C_or(1,:));
spikenums=zeros(d1,d2);
for i = 1:length(C_or(:,1))
    ac=att{i};
    acc = 0.5;
    result = round(ac/acc)*acc;
    result=round(result*2);
    try
    for j=1:length(result)
        spikenums(i,result(j))=1;
    end
    catch
    end
end

for i=1:length(spikenums)
    y(i)=sum(spikenums(:,i));
end

%%
%%make surrogate distribution
for j=1:1000
thresh=zeros(length(C_or(:,1)),length(spikenums(1,:)));
parfor i=1:length(C_or(:,1))
    thresh(i,:)=circshift(spikenums(i,:), randi([1, length(spikenums(1,:))]));
end
summ=sum(thresh,1);
summ2{j}=summ;
end
tress = cell2mat(summ2);
thresh = prctile(tress,99); %or 95 probably doesn't matter

[pks,locs] = findpeaks(y,'MinPeakDistance',7,'MinPeakHeight',thresh);

figure;
imagesc(spikenums)
hold on;
plot(y,'color','r')

findpeaks(y,'MinPeakDistance',7,'MinPeakHeight',thresh);

%%
%postprocessing
lb=3; %frames before
hb=3; %frames after vector of concatenated vector should be center at 0
if (locs(end)+hb)>length(spikenums(1,:))
    locs=locs(1:end-1);
    pks=pks(1:end-1);
end

if (locs(1)-lb)<1
    locs=locs(2:end);
    pks=pks(2:end);
end


t1=NaN(length(locs),length(att));
t2=zeros(length(locs),length(att));


for i=1:length(pks)
    for j=1:length(att)
        axxx=spikenums(j,locs(i)-lb:locs(i)+hb);
        ax=min(find(axxx==1));
        if ax>=1
            t1(i,j)=ax;
        end
    end
end

tttt=round(length(-lb:hb)/2);
t11=t1-tttt;
t12=t11;
t12(~isnan(t11))=1;
t12(isnan(t12))=0;

%%
%start cluster algo, run time should probably be improved
Race = t12';
[NCell,NRace] = size(Race);
[IDX2,sCl,M,S] = kmeansopt(Race,100,'var');
% M = CovarM(Race);
% IDX2 = kmedoids(M,NCl);
NCl = max(IDX2);

[~,x2] = sort(IDX2);
MSort = M(x2,x2);

%Race clusters
R = cell(0);
CellScore = zeros(NCell,NCl);
CellScoreN = zeros(NCell,NCl);
for i = 1:NCl
    R{i} = find(IDX2==i);
    CellScore(:,i) = sum(Race(:,R{i}),2);
    CellScoreN(:,i) = CellScore(:,i)/length(R{i});
end
%Assign cells to cluster with which it most likely spikes
[~,CellCl] = max(CellScoreN,[],2);
%Remove cells with less than 2 spikes in a given cluster
CellCl(max(CellScore,[],2)<2) = 0;
[X1,x1] = sort(CellCl);

figure
subplot(1,2,1)
imagesc(MSort)
colormap jet
axis image
xlabel('RACE #')
ylabel('RACE #')

subplot(1,2,2)
imagesc(Race(x1,x2),[-1 1.2])
axis image
xlabel('RACE #')
ylabel('Cell #')
SS = FileName(1:end-4);
SSS='Clusters.fig';
St=strcat(SS,SSS);

savefig(St)

%% Save Clusters
SS = FileName(1:end-4);
SSS='Clusters.mat';
St=strcat(SS,SSS);
save(St,'IDX2')


%% Remove cluster non-statistically significant

sClrnd = zeros(1,1000);
for i = 1:1000
    sClrnd(i) = kmeansoptrnd(Race,10,NCl);
end
% TODO: replace max by percentile (done)
NClOK = sum(sCl>prctile(sClrnd,95));
sClOK = sCl(1:NClOK)';

%save('NClustersOK.mat','NClOK')

RaceOK = Race(:,IDX2<=NClOK);
NRaceOK = size(RaceOK,2);

%% Statistical definition of cell assemblies

NShuf = 5000;
%Count number of participation to each cluster
CellP = zeros(NCell,NCl); CellR = zeros(NCell,NCl);
for i = 1:NCl
    CellP(:,i) = sum(Race(:,IDX2 == i),2);
    CellR(:,i) = CellP(:,i)/sum(IDX2 == i);
end

%Test for statistical significance
CellCl = zeros(NCl,NCell); %Binary matrix of cell associated to clusters
for j = 1:NCell
    %Random distribution among Clusters
    RClr = zeros(NCl,NShuf);
    Nrnd = sum(Race(j,:));
    parfor l = 1:NShuf
        Random = randperm(NRace);
        Random = Random(1:Nrnd);
        Racer = zeros(1,NRace);
        Racer(Random) = 1;
        for i = 1:NCl
            RClr(i,l) = sum(Racer(:,IDX2 == i),2);
        end
    end
    RClr = sort(RClr,2);
    %         ThMin = mean(Random) - 2*std(Random);
    %Proba above 95th percentile
    ThMax = RClr(:,round(NShuf*(1-0.05/NCl))); 
    for i = 1:NCl
        CellCl(i,j) = double(CellP(j,i)>ThMax(i));% - double(RCl(:,j)<ThMin);
    end
end

A0 = find(sum(CellCl) == 0); %Cells not in any cluster
A1 = find(sum(CellCl) == 1); %Cells in one cluster
A2 = find(sum(CellCl) >= 2); %Cells in several clusters

%Keep cluster where they participate the most
for i = A2
    [~,idx] = max(CellR(i,:));
    CellCl(:,i) = 0;
    CellCl(idx,i) = 1;
end
C0 = cell(0);
k = 0;
for i = 1:NCl
    if length(find(CellCl(i,:)))>5
        k = k+1;
        C0{k} = find(CellCl(i,:));
    end
end

SS = FileName(1:end-4);
SSS='CellList.mat';
St=strcat(SS,SSS);
save(St,'C0');

%Participation rate to its own cluster
SS = FileName(1:end-4);
SSS='ClusPart.mat';
St=strcat(SS,SSS);
CellRCl = max(CellR([A1 A2],:),[],2);
save(St,'CellRCl')
%% Assign RACE to groups of cells

NCl = length(C0);
[NCell,NRace] = size(Race);
%Cell count in each cluster
RCl = zeros(NCl,NRace);
PCl = zeros(NCl,NRace);
for i = 1:NCl
    RCl(i,:) = sum(Race(C0{i},:));
end

RCln = zeros(NCl,NRace);
for j = 1:NRace
    %Random distribution among Clusters
    RClr = zeros(NCl,NShuf);
    Nrnd = sum(Race(:,j));
    parfor l = 1:NShuf
        Random = randperm(NCell);
        Random = Random(1:Nrnd);
        Racer = zeros(NCell,1);
        Racer(Random) = 1;
        for i = 1:NCl
            RClr(i,l) = sum(Racer(C0{i}));
        end
    end
    %         ThMin = mean(Random) - 2*std(Random);
    RClr = sort(RClr,2);
    %         ThMin = mean(Random) - 2*std(Random);
    %Proba above 95th percentile
    ThMax = RClr(:,round(NShuf*(1-0.05/NCl)));
    for i = 1:NCl
        PCl(i,j) = double(RCl(i,j)>ThMax(i));% - double(RCl(:,j)<ThMin);
    end
    %Normalize (probability)
    RCln(:,j) = RCl(:,j)/sum(Race(:,j));
end

SS = FileName(1:end-4);
SSS='CellCluster.mat';
St=strcat(SS,SSS);

save(St,'PCl');

%% Show Sorted Rasterplot

%load('RaceCl2')
%load('CellClList2.mat')
%load('Race.mat')
[NCell,NRace] = size(Race);
NCl = length(C0);

%Recreate CellCl (equivalent of RCl for the cells)
CellCl = zeros(NCl,NCell);
for i = 1:NCl
    CellCl(i,C0{i}) = 1;
end

NCl = length(C0);

Cl0 = find(sum(PCl,1) == 0);
Cl1 = find(sum(PCl,1) == 1);
Cl2 = find(sum(PCl,1) == 2);
Cl3 = find(sum(PCl,1) == 3);
Cl4 = find(sum(PCl,1) == 4);

Bin = 2.^(0:NCl-1);

%Sort Cl1
[~,x01] = sort(Bin*PCl(:,Cl1));
Cl1 = Cl1(x01);

%Sort Cl2
[~,x02] = sort(Bin*PCl(:,Cl2));
Cl2 = Cl2(x02);

%Sort Cl3
[~,x03] = sort(Bin*PCl(:,Cl3));
Cl3 = Cl3(x03);

RList = [Cl0 Cl1 Cl2 Cl3 Cl4];
%x1 from DetectRace

[X1,x1] = sort(Bin*CellCl);

figure
imagesc(Race(x1,RList))
colormap hot
axis image

SS = FileName(1:end-4);
SSS='CellCluster.fig';
St=strcat(SS,SSS);
savefig(St)
close all;

SS = FileName(1:end-4);
SSS='all.mat';
St=strcat(SS,SSS);

save(St,'-v7.3');
