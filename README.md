# Code Modol et al. 2020

Run either auto.m (recommended) or manual.m to replicate calcium processing and cluster analysis as described in the paper 
"Assemblies of Perisomatic GABAergic Neurons in the Developing Barrel Cortex"
by Modol et al. (https://doi.org/10.1016/j.neuron.2019.10.007).

You will also need to add the following repositories:

https://github.com/flatironinstitute/CaImAn-MATLAB

https://github.com/flatironinstitute/NoRMCorre

http://cvxr.com/cvx/download/

Data will be added soon on Mendeley.